package application;


public class RectangularSection {

	public double widthRectangle;
 	public double heightRectangle;
 	public double distanceArectangle;
 	public Double[] strengthClassOfConcrete;
 	public double strengthClassOfReinforcingSteel;
 	public double strengthClassOfStirrups;
	
	public double getWidthRectangle() {
		return widthRectangle;
	}

	public double getStrengthClassOfStirrups() {
		return strengthClassOfStirrups;
	}

	public void setStrengthClassOfStirrups(double strengthClassOfStirrups) {
		this.strengthClassOfStirrups = strengthClassOfStirrups;
	}

	public double getStrengthClassOfReinforcingSteel() {
		return strengthClassOfReinforcingSteel;
	}

	public void setStrengthClassOfReinforcingSteel(double strengthClassOfReinforcingSteel) {
		this.strengthClassOfReinforcingSteel = strengthClassOfReinforcingSteel;
	}

	public Double[] getStrengthClassOfConcrete() {
		return strengthClassOfConcrete;
	}
	
	public Double getValueFromStrengthClassOfConcrete(Integer value){
		return strengthClassOfConcrete[value];
	}

	public void setStrengthClassOfConcrete(Double[] strengthClassOfConcrete) {
		this.strengthClassOfConcrete = strengthClassOfConcrete;
	}

	public void setWidthRectangle(double widthRectangle) {
		this.widthRectangle = widthRectangle;
	}

	public double getHeightRectangle() {
		return heightRectangle;
	}

	public void setHeightRectangle(double heightRectangle) {
		this.heightRectangle = heightRectangle;
	}

	public double getDistanceArectangle() {
		return distanceArectangle;
	}

	public void setDistanceArectangle(double distanceArectangle) {
		this.distanceArectangle = distanceArectangle;
	}

}