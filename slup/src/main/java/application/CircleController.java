package application;


import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

public class CircleController implements Initializable {
	
	@FXML
	private TextField diameterColumn;
	
	@FXML
	private TextField distanceAcircle;
	
	
	public TextField getDiameterColumn() {
		return diameterColumn;
	}

	public void setDiameterColumn(TextField diameterColumn) {
		this.diameterColumn = diameterColumn;
	}

	public TextField getDistanceAcircle() {
		return distanceAcircle;
	}
	
	public void setDistanceAcircle(TextField distanceAcircle) {
		this.distanceAcircle = distanceAcircle;
	}




	public void initialize(URL arg0, ResourceBundle arg1) {

	}
}
