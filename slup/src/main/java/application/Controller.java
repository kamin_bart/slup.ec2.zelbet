package application;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Controller implements Initializable  {
	
	ParametersColumn parameters = new ParametersColumn();
	RectangularSection rectangle = new RectangularSection();
	RectangleController sectionRectangleController = new RectangleController();
	
	String[] listSteelClass = {"B 400 A","B 400 B","B 400 C","B 450 A","B 450 B","B 450 C","B 500 A","B 500 B",
							   "B 500 C","B 550 A","B 550 B","B 550 C","B 600 A","B 600 B","B 600 C"};
	
	String[] listDiameterBar = {"4","5","6","8","10","12","14","16","18","20","22","25","28","32","36","40"};
	
	String[] listexposureClass = {"X0","XC1","XC2/XC3","XC4","XD1/XS1","XD2/XS2","XD3/XS3"};
	
	String[] listStrengthClassOfConcrete = {"C12/15","C16/20","C20/25","C25/30","C30/37","C35/45","C40/50",
											"C45/55","C50/60","C55/67","C60/75","C70/85","C80/95","C90/105"};
	
	@FXML
	private Pane rectangleOrCirclePanel;
	
	@FXML
	private ComboBox<String> strengthClassOfConcrete;
	
	@FXML
	private ComboBox<String> diameterOfAReinforcingBar;
	
	@FXML
	private ComboBox<String> diameterOfAStirrups;
	
	@FXML
	private ComboBox<String> strengthClassOfReinforcingSteel;
	
	@FXML
	private ComboBox<String> strengthClassOfStirrups;
	
	@FXML
	private ComboBox<String> exposureClass;
	
	@FXML
	private Tab resultsPanel;
	
	@FXML
	private Label szerokosc;
	@FXML
	private Label wysokosc;
	@FXML
	private Label odlegloscA;
	@FXML
	private Label beton;
	@FXML
	private Label danaFck;
	@FXML
	private Label stalZbrojenia;
	@FXML
	private Label stalStrzemion;
	
	
	@FXML
	private void choiceRectangle(ActionEvent event) throws IOException{
			Pane newLoadedPane =  FXMLLoader.load(getClass().getResource("/application/rectangle.fxml"));
			if(rectangleOrCirclePanel.getChildren().isEmpty()){
			rectangleOrCirclePanel.getChildren().add(newLoadedPane);	
			}
		else{
			rectangleOrCirclePanel.getChildren().clear();
			rectangleOrCirclePanel.getChildren().add(newLoadedPane);
			}
			
	}
	@FXML
	private void choiceCircle(ActionEvent event) throws IOException{
		Pane newLoadedPane =  FXMLLoader.load(getClass().getResource("/application/circle.fxml"));
		if(rectangleOrCirclePanel.getChildren().isEmpty()){
			rectangleOrCirclePanel.getChildren().add(newLoadedPane);	
			}
		else{
			rectangleOrCirclePanel.getChildren().clear();
			rectangleOrCirclePanel.getChildren().add(newLoadedPane);
			}
	}
	

	
	@FXML
	private void sendStrengthClassOfConcrete() {
		rectangle.setWidthRectangle(sectionRectangleController.getFieldWidthRectangle());
		szerokosc.setText(String.valueOf(rectangle.getWidthRectangle()));

		rectangle.setHeightRectangle(sectionRectangleController.getFieldHeightRectangle());
		wysokosc.setText(String.valueOf(rectangle.getHeightRectangle()));

		rectangle.setDistanceArectangle(sectionRectangleController.getFieldDistanceArectangle());
		odlegloscA.setText(String.valueOf(rectangle.getDistanceArectangle()));

		beton.setText(strengthClassOfConcrete.getValue());
		
		rectangle.setStrengthClassOfConcrete(parameters.getTableFromStrengthClassOfConcrete(strengthClassOfConcrete.getValue()));
		danaFck.setText(String.valueOf(rectangle.getValueFromStrengthClassOfConcrete(0)));	
		
		rectangle.setStrengthClassOfReinforcingSteel(parameters.getValueStrengthClassOfReinforcingSteel(strengthClassOfReinforcingSteel.getValue().substring(0, 5)));
		stalZbrojenia.setText(String.valueOf(rectangle.getStrengthClassOfReinforcingSteel()));
		
		rectangle.setStrengthClassOfStirrups((parameters.getValueStrengthClassOfReinforcingSteel(strengthClassOfStirrups.getValue().substring(0, 5))));
		stalStrzemion.setText(String.valueOf(rectangle.getStrengthClassOfStirrups()));
	}
	
	@FXML
	void openExposureWinow() throws IOException{
		Stage stage = new Stage();
		stage.setTitle("Otulina");
		Parent root = FXMLLoader.load(getClass().getResource("/application/ExposureWindow.fxml"));
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
	}
	

	public void initialize(URL arg0, ResourceBundle arg1) {
		
		try {
			Pane newLoadedPane = FXMLLoader.load(getClass().getResource("/application/rectangle.fxml"));
			rectangleOrCirclePanel.getChildren().add(newLoadedPane);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		strengthClassOfConcrete.setValue("C20/25");
		strengthClassOfConcrete.getItems().addAll(listStrengthClassOfConcrete);
		diameterOfAReinforcingBar.setValue("12");
		diameterOfAReinforcingBar.getItems().addAll(listDiameterBar);
		diameterOfAStirrups.setValue("6");
		diameterOfAStirrups.getItems().addAll(listDiameterBar);
		strengthClassOfReinforcingSteel.setValue("B 400 A");
		strengthClassOfReinforcingSteel.getItems().addAll(listSteelClass);
		strengthClassOfStirrups.setValue("B 400 A");
		strengthClassOfStirrups.getItems().addAll(listSteelClass);
		exposureClass.setValue("X0");
		exposureClass.getItems().addAll(listexposureClass);
		
	}}



