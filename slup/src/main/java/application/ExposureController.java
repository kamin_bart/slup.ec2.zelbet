package application;


import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class ExposureController {

	@FXML Button acceptButton;
	@FXML Button belayButton;
	
	@FXML
	void openExposureWinow() throws IOException{
	Stage stage = new Stage();
	stage.setTitle("S�up �elbetowy EC2");
	Parent root = FXMLLoader.load(getClass().getResource("/application/ExposureWindow.fxml"));
	Scene scene = new Scene(root);
	stage.setScene(scene);
	stage.show();
	}

	
}
