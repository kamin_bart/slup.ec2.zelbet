package application;


import java.util.HashMap;
import java.util.Map;

public class ParametersColumn {

	Map<String, Double[]> strengthClassOfConcrete= new HashMap<String, Double[]>();
	Map<String, Double> strengthClassOfReinforcingSteel= new HashMap<String, Double>();	
	
	ParametersColumn(){
		strengthClassOfConcrete.put("C12/15", new Double[] {12.0, 15.0, 20.0, 1.6, 1.1, 2.0, 27.0, 1.8, 3.5, 2.0, 3.5, 2.0, 1.75, 3.5 });
		strengthClassOfConcrete.put("C16/20", new Double[] {16.0, 20.0, 24.0, 1.9, 1.3, 2.5, 29.0, 1.9, 3.5, 2.0, 3.5, 2.0, 1.75, 3.5 });
		strengthClassOfConcrete.put("C20/25", new Double[] {20.0, 25.0, 28.0, 2.2, 1.5, 2.9, 30.0, 2.0, 3.5, 2.0, 3.5, 2.0, 1.75, 3.5 });
		strengthClassOfConcrete.put("C25/30", new Double[] {25.0, 30.0, 33.0, 2.6, 1.8, 3.3, 31.0, 2.1, 3.5, 2.0, 3.5, 2.0, 1.75, 3.5 });
		strengthClassOfConcrete.put("C30/37", new Double[] {30.0, 37.0, 38.0, 2.9, 2.0, 3.8, 32.0, 2.2, 3.5, 2.0, 3.5, 2.0, 1.75, 3.5 });
		strengthClassOfConcrete.put("C35/45", new Double[] {35.0, 45.0, 43.0, 3.2, 2.2, 4.2, 34.0, 2.25, 3.5, 2.0, 3.5, 2.0, 1.75, 3.5 });
		strengthClassOfConcrete.put("C40/50", new Double[] {40.0, 50.0, 48.0, 3.5, 2.5, 4.6, 35.0, 2.3, 3.5, 2.0, 3.5, 2.0, 1.75, 3.5 });
		strengthClassOfConcrete.put("C45/55", new Double[] {45.0, 55.0, 53.0, 3.8, 2.7, 4.9, 36.0, 2.4, 3.5, 2.0, 3.5, 2.0, 1.75, 3.5 });
		strengthClassOfConcrete.put("C50/60", new Double[] {50.0, 60.0, 58.0, 4.1, 2.9, 5.3, 37.0, 2.45, 3.5, 2.0, 3.5, 2.0, 1.75, 3.5 });
		strengthClassOfConcrete.put("C55/67", new Double[] {55.0, 67.0, 63.0, 4.2, 3.0, 5.5, 38.0, 2.5, 3.2, 2.2, 3.1, 1.75, 1.8, 3.1 });
		strengthClassOfConcrete.put("C60/75", new Double[] {60.0, 75.0, 68.0, 4.4, 3.1, 5.7, 39.0, 2.6, 3.0, 2.3, 2.9, 1.6, 1.9, 2.9 });
		strengthClassOfConcrete.put("C70/85", new Double[] {70.0, 85.0, 78.0, 4.6, 3.2, 6.0, 41.0, 2.7, 2.8, 2.4, 2.7, 1.45, 2.0, 2.7 });
		strengthClassOfConcrete.put("C80/95", new Double[] {80.0, 95.0, 88.0, 4.8, 3.4, 6.3, 42.0, 2.8, 2.8, 2.5, 2.6, 1.4, 2.2, 2.6 });
		strengthClassOfConcrete.put("C90/105", new Double[] {90.0, 105.0, 98.0, 5.0, 3.5, 6.6, 44.0, 2.8, 2.8, 2.6, 2.6, 1.4, 2.3, 2.6 });
		
		strengthClassOfReinforcingSteel.put("B 400", 400.0);
		strengthClassOfReinforcingSteel.put("B 450", 450.0);
		strengthClassOfReinforcingSteel.put("B 500", 500.0);
		strengthClassOfReinforcingSteel.put("B 550", 550.0);
		strengthClassOfReinforcingSteel.put("B 600", 600.0);
	}

	void showTabStrengthClassOfConcrete(String concreteClass){
		Double[] table = strengthClassOfConcrete.get(concreteClass);
		for(double x : table){
		System.out.println(x);	
		}
	}
	
	Double getValueFromStrengthClassOfConcrete(String concreteClass, Integer value){
		Double[] table = strengthClassOfConcrete.get(concreteClass);
		return table[value];
	}
	
	Double[] getTableFromStrengthClassOfConcrete(String concreteClass){
		return strengthClassOfConcrete.get(concreteClass);
	}
	
	double getValueStrengthClassOfReinforcingSteel(String steel){
		return strengthClassOfReinforcingSteel.get(steel);
	}
	



public static void main (String[] args) throws java.lang.Exception{
	ParametersColumn objekt = new ParametersColumn();
	objekt.getValueFromStrengthClassOfConcrete("C20/25", 4);
	System.out.println(objekt.getTableFromStrengthClassOfConcrete("C55/67"));
}
}