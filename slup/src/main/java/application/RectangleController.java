package application;


import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

public class RectangleController implements Initializable {

	@FXML
	private TextField widthRectangle;

	@FXML
	private TextField heightRectangle;

	@FXML
	private TextField distanceArectangle;
	
	static private double valueWidthRectangle;
	static private double valueHeightRectangle;
	static private double valueDistanceArectangle;

	RectangularSection sectionRectangle = new RectangularSection();
	

	RectangularSection Object1() {
		return this.sectionRectangle;
	}

	
	public Double getFieldWidthRectangle() {
		return valueWidthRectangle;
	}
	

	public Double getFieldHeightRectangle() {
		return valueHeightRectangle;
	}
	

	public Double getFieldDistanceArectangle() {
		return valueDistanceArectangle;
	}
	

	@FXML
	public void sendWidthRectangle() {
		valueWidthRectangle = Double.parseDouble(widthRectangle.getText());
	}

	
	@FXML
	public void sendHeightRectangle() {
		valueHeightRectangle = Double.parseDouble(heightRectangle.getText());
	}

	
	@FXML
	public void sendDistanceArectangle() {
		valueDistanceArectangle = Double.parseDouble(distanceArectangle.getText());
	}

	
	
	public void initialize(URL arg0, ResourceBundle arg1) {
	}
}
